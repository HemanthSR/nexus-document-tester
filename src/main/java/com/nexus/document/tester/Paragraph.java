package com.nexus.document.tester;

public class Paragraph {
    private String paragraphText;

    public Paragraph(String paragraphText) {
        this.paragraphText = paragraphText;
    }

    public String getParagraphText() {
        return paragraphText;
    }

    public boolean isValid() {
        if (paragraphText.trim().startsWith("\"") || paragraphText.trim().startsWith(".")) {
            if(paragraphText.startsWith("     ")) {
                System.out.println("ERROR:");
                System.out.println("If paragraph is starting with inverted comma(“ “), or Dots (....) do not apply 5 spaces.)\n" +
                        "Eg: (ROOSWELL: This is happened..............)\n");
                System.out.println(paragraphText);
                return false;
            }
            return true;
        }
        if (hasMoreThanOneFullStop()
                || hasMoreThanOneQuestionMark()
                || hasMoreThanOneExclamationMark()
                || hasFullColonAfterFullStop()
                || hasSemiColonAfterFullStop()
                || hasHyphenAfterFullStop()) {
            if (!paragraphText.startsWith("     ")){
                System.out.println("ERROR:");
                System.out.println("Every new paragraph must start with 5 Space (Do not use tab instead of five space).\n");
                System.out.println("Consider Paragraph If More then One Full stop, Question mark or Exclamation mark containing in lines, or full stop coming before any (:) (;) or (-), otherwise consider it as a single line sentence.\n");
                System.out.println(paragraphText);
                return false;
            }
            return true;
        }
        if (paragraphText.startsWith("     ")){
            System.out.println("ERROR:");
            System.out.println("If it is a one line sentence. (Do not apply five space at starting.)");
            System.out.println(paragraphText);
            return false;
        }
        return true;
    }

    private boolean contains(String string){
        return paragraphText.contains(string);
    }
    private boolean hasHyphenAfterFullStop() {
        return contains(".") && contains("-") && (paragraphText.indexOf(".") < paragraphText.indexOf("-"));
    }

    private boolean hasSemiColonAfterFullStop() {
        return contains(".") && contains(";") && (paragraphText.indexOf(".") < paragraphText.indexOf(";"));
    }

    private boolean hasFullColonAfterFullStop() {
        return contains(".") && contains(":") && (paragraphText.indexOf(".") < paragraphText.indexOf(":"));
    }

    private boolean hasMoreThanOneExclamationMark() {
        return paragraphText.indexOf("!") != paragraphText.lastIndexOf("!");
    }

    private boolean hasMoreThanOneQuestionMark() {
        return paragraphText.indexOf("?") != paragraphText.lastIndexOf("?");
    }

    private boolean hasMoreThanOneFullStop() {
        return paragraphText.indexOf(".") != paragraphText.lastIndexOf(".");
    }

    public boolean isEmpty() {
        return paragraphText.equals("");
    }
}
