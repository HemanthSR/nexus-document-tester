package com.nexus.document.tester;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DocumentFileReader {
    public List<Paragraph> read(String fileName) {
        File file = new File(fileName);
        FileInputStream fis = null;
        List<Paragraph> paragraphList = new ArrayList<Paragraph>();
        try {
            fis = new FileInputStream(file.getAbsolutePath());
            XWPFDocument document = new XWPFDocument(fis);

            List<XWPFParagraph> paragraphs = document.getParagraphs();

            for (XWPFParagraph paragraph : paragraphs) {
                System.out.println(paragraph.getText());
                paragraphList.add(new Paragraph(paragraph.getText()));
            }
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return paragraphList;
    }
}
