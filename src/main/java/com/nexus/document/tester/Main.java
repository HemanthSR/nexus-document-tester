package com.nexus.document.tester;

import java.io.IOException;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {
        System.out.println(args[0]);
        DocumentFileReader documentFileReader = new DocumentFileReader();
        List<Paragraph> paragraphs = documentFileReader.read(args[0]);
        DocumentTester documentTester = new DocumentTester(paragraphs);
        documentTester.test();
    }
}
