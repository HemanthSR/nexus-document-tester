package com.nexus.document.tester;

import java.util.List;

public class DocumentTester {
    private List<Paragraph> paragraphs;

    public DocumentTester(List<Paragraph> paragraphs) {
        this.paragraphs = paragraphs;
    }

    public boolean test() {
        if (!paragraphs.get(1).isEmpty()){
            System.out.println("Error in paragraph : 1");
            System.out.println("Give line break after heading.");
            return false;
        }
        for (Paragraph paragraph : paragraphs) {
            if (paragraphs.indexOf(paragraph)>1){
                if(paragraph.isEmpty()){
                    System.out.println("Error:");
                    System.out.println("Always maintain 1 line gap after Main Heading on the top of the page. (If there are more then one heading on page only give one line gap after first heading.)");
                    System.out.println("\nParagraph : " + paragraph.getParagraphText());
                    return false;
                }
            }
            if (paragraphs.indexOf(paragraph) > 0
                    && (paragraphs.get(paragraphs.indexOf(paragraph)-1).getParagraphText().endsWith(":")
                    || paragraphs.get(paragraphs.indexOf(paragraph)-1).getParagraphText().endsWith(";")
                    || paragraphs.get(paragraphs.indexOf(paragraph)-1).getParagraphText().endsWith("-")
                    || paragraphs.get(paragraphs.indexOf(paragraph)-1).getParagraphText().endsWith("and"))){
                if (!paragraph.getParagraphText().startsWith("     ")){
                    System.out.println("Error:");
                    System.out.println("If a paragraph or sentence is ending with, Colon ( : ) Semi-colon ( ; ) Hyphen ( -) or (and). ( Do not apply five space in next paragraph.)");
                    System.out.println("\nParagraph : " + paragraph.getParagraphText());
                    return false;
                }
            }
            if (!paragraph.isValid()) {
                return false;
            }
        }
        return true;
    }
}
